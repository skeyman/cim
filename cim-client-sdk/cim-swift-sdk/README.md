# cimsdk

[![CI Status](https://img.shields.io/travis/飞鱼/cimsdk.svg?style=flat)](https://travis-ci.org/飞鱼/cimsdk)
[![Version](https://img.shields.io/cocoapods/v/cimsdk.svg?style=flat)](https://cocoapods.org/pods/cimsdk)
[![License](https://img.shields.io/cocoapods/l/cimsdk.svg?style=flat)](https://cocoapods.org/pods/cimsdk)
[![Platform](https://img.shields.io/cocoapods/p/cimsdk.svg?style=flat)](https://cocoapods.org/pods/cimsdk)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

cimsdk is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'cimsdk'
```

## Author

飞鱼, 870027381@qq.com

## License

cimsdk is available under the MIT license. See the LICENSE file for more info.
